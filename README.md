# Arduino Nano EagleCAD Library

Arduino Nano cihazının EagleCAD üzerinde hazırlanmış olan kütüphanesidir. Kütüphaneyi indirdikten sonra EagleCAD üzerinde ilgili /libraries dizininin altına atılması gerekir.

Bu işlem gerçekleştirildikten sonra EagleCAD Control Panel üzerinde ArduinoNano.lbr kütüphanesinin üzerine sağ tıklanarak Use seçeneği işaretlenir.

İşlemlerin tamamlanmasıyla kütüphane hazır kullanılabilir duruma gelecektir. Devre çizim ekranında Add Part seçeneğiyle Arduino Nano seçilerek projeye kolaylıkla dahil edilebilir.

Kütüphanenin çizimi gerçekleştirilirken Arduino Nano' nun gerçek ölçüleri esas alınmıştır. Bu ölçüler aşağıdaki gibidir.

[Arduino Nano Mechanical Drawing](https://gitlab.com/berkayaltass/arduino-nano-eagle-library/-/blob/master/arduinonanomechanicaldrawing.jpg)

Kütüphanenin EagleCAD' de Schematic üzerinde görünümü : 

[Arduino Nano EagleCAD Schematic Görünüm](https://gitlab.com/berkayaltass/arduino-nano-eagle-library/-/blob/master/schematicgorunum.png)

Kütüphanenin EagleCAD' de PCB üzerinde görünümü : 

![Arduino Nano EagleCAD PCB Görünüm](https://gitlab.com/berkayaltass/arduino-nano-eagle-library/-/blob/master/pcbgorunum.png)

